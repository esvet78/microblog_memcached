module.exports.randomStr = function randomStr(min, max) {
    var len = ~~(Math.random() * (max - min) + min);
    var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz           ';
    var res = '';
    for (let i = 0; i < len; i++) {
        res += chars.charAt(~~(Math.random() * chars.length));
    }
    return res;
}
