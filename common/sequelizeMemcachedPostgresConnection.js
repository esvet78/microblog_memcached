const
    Sequelize = require('sequelize'),
    initCache = require('sequelize-cacher'),
    config = require('../config/config'),
    Memcached = require('memcached'),
    cacheEngine = new Memcached(config.memcachedHost + ':' + config.memcachedPort);
//     MemcachedAdaptor = require('sequelize-transparent-cache-memcached'),
//     memcachedAdaptor = new MemcachedAdaptor({
//     client: memcached,
//     lifetime: 60 * 60
// })
module.exports.Sequelize = Sequelize;

sequelize = module.exports.sequelize =
    new Sequelize('postgres://' + config.pgUser + ':' + config.pgPassword + '@' +
        config.pgHost + ':' + config.pgPort + '/' + config.pgDBName, {logging: false});
module.exports.cacher = initCache(sequelize, cacheEngine);

//module.exports.cacher = cache('post').ttl(5);
