const
    dto = require('../common/requireDir')('../dto'),
    Post = require('../common/sequelizeModels').Post,
    User = require('../common/sequelizeModels').User,
    Vote = require('../common/sequelizeModels').Vote,
    genPosts = require('../config/config').genPosts,
    genUsers = require('../config/config').genUsers,
    genVotes = require('../config/config').genVotes,
    util = require('../common/util');


module.exports = {

    generatePostTable: function (callback, error) {
        Post.sync({force: genPosts.createPostTable}).then(
            () => callback('Post table was created'),
            (err) => error(err)
        )
    },

    generateUserTable: function (callback, error) {
        User.sync({force: genUsers.createUserTable}).then(
            () => callback('User table was created'),
            (err) => error(err)
        )
    },

    generateVoteTable: function (callback, error) {
        Vote.sync({force: genVotes.createVoteTable}).then(
            () => callback('Vote table was created'),
            (err) => error(err)
        )
    },

    generatePosts: function (quantity, callback, error) {

        var postArr = [];

        for (var i = 0; i < quantity; i++) {
            var userId = Math.floor(Math.random() * (genUsers.maxUsers));
            var title = util.randomStr(genPosts.minTitleSize, genPosts.maxTitleSize);
            var text = util.randomStr(genPosts.minTextSize, genPosts.maxTextSize);
            var date = genPosts.minDate + Math.floor(Math.random() * (genPosts.maxDate - genPosts.minDate + 1));
            var upvotes = Math.floor(Math.random() * (genPosts.maxUpvotes));
            var downvotes = Math.floor(Math.random() * (genPosts.maxDownvotes));
            var rate = date + (upvotes * genPosts.upvoteEquilsSeconds -
                downvotes * genPosts.downvoteEquilsSeconds);

            var post = Post.create({
                userId: userId,
                date: date,
                title: title,
                text: text,
                upvotes: upvotes,
                downvotes: downvotes,
                rate: rate
            }).then(
                (p) => {
                    generateVotes(p);
                    console.log('Post ' + p.id + ' was created')
                },
                (err) => error(err),
            )

            postArr.push(post);
        }
        Promise.all(postArr).then(() => callback(quantity + ' posts were created'))
    },

    generateUsers: function (quantity, callback, error) {

        for (var i = 0; i < quantity; i++) {

            var name = util.randomStr(genUsers.minNameSize, genUsers.maxNameSize);
            var password = util.randomStr(genUsers.minPasswordSize, genUsers.maxPasswordSize);

            User.create({
                name: name,
                password: password,
            }).then(
                (u) => console.log('User ' + u.id + ' was created'),
                (err) => error(err)
            );
        }
        callback('Adding of ' + quantity + ' users')

    }
}

function generateVotes(data) {

    var quantity = data.upvotes + data.downvotes;

    for (var i = 0; i < quantity; i++) {

        var userId = Math.floor(Math.random() * (genUsers.maxUsers));
        var postId = data.id;

        Vote.create({
            userId: userId,
            postId: postId,
        }).then((v) => console.log('Vote ' + v.id + ' for post ' + postId + ' was created'));
    }
}

