const
    Sequelize = require('sequelize');
module.exports.Sequelize = Sequelize;
sequelize = module.exports.sequelize = new Sequelize('postgres://gis:password@localhost:5432/gis',{logging: true});

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });
