module.exports = class Post {
    constructor(id, userId, date, title, text, upvotes, downvotes, rate) {
        this.id = id;
        this.userId = userId;
        this.date = date;
        this.title = title;
        this.text = text;
        this.upvotes = upvotes;
        this.downvotes = downvotes;
        this.rate = rate;
    }
};

