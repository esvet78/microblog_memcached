const
    express = require('express'),
    controllers = require('./common/requireDir')('../controller'),
    config = require('./config/config'),
    gen = require('./common/generateEntities'),
    router = express.Router();

function auth() {
    return function (req, res, next) {
        return (req.session.currUser) ? next() : res.sendStatus(403);
    };
}

router.get('/ping', (req, res) => res.status(200).end('pong'));

router.post('/register',
    controllers.userController.register);
router.post('/authenticate',
    controllers.userController.login);
router.get('/logout', auth(),
    controllers.userController.logout);

router.get('/post/:id', auth(),
    controllers.postController.get);
router.get('/post',
    controllers.postController.getTop);
router.post('/post', auth(),
    controllers.postController.create);
router.patch('/post/:id', auth(),
    controllers.postController.update);
router.get('/post/upvote/:id', auth(),
    controllers.postController.upvote);
router.get('/post/downvote/:id', auth(),
    controllers.postController.downvote);
router.delete('/post/:id', auth(),
    controllers.postController.remove);



router.get('/generate/voteTable', function (req, res) {
    gen.generateVoteTable(
        (success) => res.status(200).json(success),
        (err) => res.status(520).json(err))
});

router.get('/generate/userTable', function (req, res) {
    gen.generateUserTable(
        (success) => res.status(200).json(success),
        (err) => res.status(520).json(err))
});

router.get('/generate/postTable', function (req, res) {
    gen.generatePostTable(
        (success) => res.status(200).json(success),
        (err) => res.status(520).json(err))
});

router.get('/generate/posts', function (req, res) {
    gen.generatePosts(config.genPosts.maxPosts,
        (success) => res.status(200).json(success),
        (err) => res.status(520).json(err))
});

router.get('/generate/users', function (req, res) {
    gen.generateUsers(config.genUsers.maxUsers,
        (success) => res.status(200).json(success),
        (err) => res.status(520).json(err))
});


module.exports = router;
