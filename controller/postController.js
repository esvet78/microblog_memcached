const postModel = require('../model/postModel'),
    dto = require('../common/requireDir')('../dto');

module.exports = new PostsController();

function PostsController() {

    this.get = (req, res) => {
        var id = req.params.id;
        postModel.get(id,
            (user) => {
                res.status(200).json(user);
                res.end();
            },
            (err) => {
                res.status(520).json(err);
                res.end();
            }
        );
    };

    this.getTop = (req, res) => {
        var data = req.query;
        postModel.getTop(data,
            (users) => {
                res.status(200).json(users);
                res.end();
            },
            (err) => {
                res.status(520).json(err);
                res.end();
            }
        );
    };

    this.create = (req, res) => {
        if (req.body == undefined) {
            res.status(400).json('Post was not created');
            res.end();
            return;
        }
        var post = req.body;
        postModel.create(post, req.session.currUser,
            (id) => {
                res.status(201).json(id);
                res.end();
            },
            (err) => {
                res.status(520).json(err);
                res.end();
            }
        );
    };

    this.update = (req, res) => {
        if (req.body == undefined) {
            res.status(400).json('Post was not updated');
            res.end();
            return;
        }
        var data = {
            id: req.params.id,
            text: req.body.text
        }
        postModel.update(data, req.session.currUser,
            (success) => {
                res.status(202).json(success);
                res.end();
            },
            (err) => {
                res.status(520).json(err);
                res.end();
            }
        );
    };

    this.upvote = (req, res) => {
        var postid = req.params.id;
        postModel.upvote(postid, req.session.currUser,
            (success) => {
                res.status(202).json(success);
                res.end();
            },
            (err) => {
                res.status(520).json(err);
                res.end();
            }
        );
    };

    this.downvote = (req, res) => {
        var postid = req.params.id;
        postModel.downvote(postid, req.session.currUser,
            (success) => {
                res.status(202).json(success);
                res.end();
            },
            (err) => {
                res.status(520).json(err);
                res.end();
            }
        );
    };

    this.remove = (req, res) => {
        var id = req.params.id;
        postModel.remove(id, req.session.currUser,
            (success) => {
                res.status(202).json(success);
                res.end();
            },
            (err) => {
                res.status(520).json(err);
                res.end();
            }
        );
    };

}