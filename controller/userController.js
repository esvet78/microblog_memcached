const userModel = require('../model/userModel'),
    dto = require('../common/requireDir')('../dto');

module.exports = new UserController();

function UserController() {

    this.register = (req, res) => {
        if (req.body == undefined) {
            res.status(400).json('User was not registered');
            res.end();
            return;
        }
        var data = req.body;
        userModel.register(data,
            (currUser) => {
                req.session.currUser = currUser;
                res.status(200).json(currUser);
                res.end();
            },
            (err) => {
                res.status(520).json(err);
                res.end();
            }
        );
    };

    this.login = (req, res) => {
        var data = req.body;
        userModel.login(data,
            (currUser) => {
                req.session.currUser = currUser;
                res.status(200).json(currUser);
                res.end();
            },
            (err) => {
                delete req.session.currUser;
                res.status(520).json(err);
                res.end();
            }
        );
    };

    this.logout = (req, res) => {
        delete req.session.currUser;
        res.status(200).json('Logged out successfully');
        res.end();
    };

}