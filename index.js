const
    http = require('http'),
    express = require('express'),
    app = express(),
//    headers = require('./common/headers'),
    router = require('./router'),
    httpServer = http.createServer(app),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    config = require('./config/config'),
    memcachedStore = require('connect-memcached')(session);

const sessionParser = session({
    store   : new memcachedStore({
        hosts: [config.memcachedHost + ':' + config.memcachedPort]
    }),
    proxy   : 'true',
    secret: 'my strong salt',
    resave: false,
    saveUninitialized: false,
    cookie: {
        //secure: true,
        maxAge: null,
        httpOnly: true
    }
});

app.use(sessionParser);

app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));

httpServer.listen(config.port);
httpServer.on('listening', onListening);

app.use(express.static(__dirname + '/front'));

app.use('/api', router);

function onListening() {
    var addr = this.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    console.log('Listening on ' + bind);
}

process.on('unhandledRejection', (err) => console.error(err));

module.exports = app;

